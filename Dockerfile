FROM node:8 as build-deps

ENV NPM_CONFIG_LOGLEVEL warn
ENV NODE_ENV production

RUN mkdir /code
WORKDIR /code
ADD . /code/

RUN npm install && npm run build


FROM nginx:1.13
COPY --from=build-deps /code/build /usr/share/nginx/html
COPY nginx.template /etc/nginx/conf.d/default.conf
EXPOSE 8001
CMD ["nginx", "-g", "daemon off;"]
