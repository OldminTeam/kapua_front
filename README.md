# Run project
1. Install Docker (https://docs.docker.com/engine/installation/)
2. Install docker-compose (https://docs.docker.com/compose/install/)
3. Build image and run containers:
```
docker build -f ./Dockerfile -t kapuafront . && docker run -it --rm -p 8001:8001 -d kapuafront
```