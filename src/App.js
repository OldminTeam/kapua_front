import React, { Component } from 'react';
import {
  Redirect,
  Route,
  BrowserRouter as Router,
  Switch
} from "react-router-dom";

import { Domain } from "./components/Domain";
import { Tree } from "./components/Tree";

import { getDomain } from "./utils";

import './App.css';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    !!getDomain() ? (
      <Component {...props}/>
    ) : (
      <Redirect to={{
        pathname: '/auth',
        state: { from: props.location }
      }}/>
    )
  )}/>
);


class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path='/auth' component={Domain}/>
          <PrivateRoute path='/' component={Tree}/>
        </Switch>
      </Router>
    );
  }
}

export default App;
