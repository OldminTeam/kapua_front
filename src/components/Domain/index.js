import React, { Component } from 'react';

import { setDomain, createObject, fetchData } from "../../utils";


export class Domain extends Component {
  state = {
    listOfDomains: [],
    yourDomain: '',
    currentDomain: ''
  };

  componentDidMount() {
    fetchData('domain/list/')
      .then((response) => this.setState({ listOfDomains: response }))
      .catch((error) => console.warn('error :', error));
  }

  _handleOnChangeDomainInput = (e) => {
    const { value } = e.target;

    this.setState({
      yourDomain: value
    });
  };

  _handleOnChangeDomainSelect = (e) => {
    let current = e.target.value.split('.')[0];
    this.setState({
      currentDomain: current
    });
  };

  _postData = () => {
    const {
      yourDomain
    } = this.state;

    createObject({ domain: yourDomain.toLowerCase() }, 'domain/list/')
      .then(() => {
        setDomain(yourDomain);
        this.props.history.push('/');
      })
      .catch((error) => console.warn('error: ', error));
  };

  _postDataChoose = () => {
    const {
      currentDomain
    } = this.state;

    setDomain(currentDomain);
    this.props.history.push('/');

  }

  render() {
    const list = this.state.listOfDomains.map((item, index) => {
      return (
        <option key={`${index}i`}>{item.domain}</option>
      )
    })

    return (
      <div className='domain-container'>
        <div className='manipulation-block'>
          <div className='content-row'>
            {/*<div className={`input-field ${errorNameParentNode ? 'border-error' : ''}`}>*/}
            <h5>Choose your domain:</h5>
            <div className={`input-field`}>
              <select onChange={this._handleOnChangeDomainSelect}>
                <option>----</option>
                {list}
              </select>
            </div>
            <div className='primaryBtn' onClick={this._postDataChoose}>Save you choice</div>
          </div>
          <div className='content-row'>
            {/*<div className={`input-field ${errorNameParentNode ? 'border-error' : ''}`}>*/}
            <h5>Write your domain:</h5>
            <div className={`input-field`}>
              <input type='text' onChange={this._handleOnChangeDomainInput}/>
            </div>
            <div className='primaryBtn' onClick={this._postData}>Save you choice</div>
          </div>
        </div>
      </div>
    )
  }
}