import React, { Component } from "react";
import
  SortableTree,
{
  addNodeUnderParent,
  changeNodeAtPath,
  removeNodeAtPath
} from "react-sortable-tree";
import FileExplorerTheme from "react-sortable-tree-theme-full-node-drag";

import 'react-sortable-tree/style.css';

import
{
  createObject,
  fetchData,
  deleteDomain
} from "../../utils";


export class Tree extends Component {
  state = {

    nameParentNode: '',
    errorNameParentNode: false,

    nameChildNode: '',
    errorNameChildNode: true,

    //subtitleNode = TestField
    subtitleNode: '',
    errorSubtitleNode: true,

    treeData: [
    ],
  };

  componentDidMount() {
    this.loadData();
  }

  loadData = () => {
    fetchData()
      .then((response) => {
        this.setState({
          treeData: response
        })
      })
      .catch((error) => {
        console.warn('error :', error);
      });
  };

  sendData = (tree) => {
    createObject(tree)
      .then(response => { })
      .catch(error => { console.warn('error: ', error)})
  }

  _AddNewNode = () => {
    const { nameParentNode } = this.state;

    let node = {
      title: nameParentNode,
      subtitle: '',
      expanded: true,
      children: []
    };

    this.setState(({ treeData }) => (
      {
        treeData: [...treeData, node],
      }), () => this.sendData(this.state.treeData));

  }

  _handleChangeTextInput = (e) => {
    const { value } = e.target;

    if(value.length !== 0) {
      this.setState({
        nameChildNode: value,
        errorNameChildNode: false
      })
    } else {
      this.setState({
        errorNameChildNode: true
      })
    }

  }

  _handleChangeSubtitleInput = (e) => {
    const { value } = e.target;

    if(value.length !== 0) {
      this.setState({
        subtitleNode: value,
        errorSubtitleNode: false
      })
    } else {
      this.setState({
        errorSubtitleNode: true
      })
    }

  }

  _handleChangeNameParentNode = (e) => {
    const { value } = e.target;

    if(value.length !== 0) {
      this.setState({
        nameParentNode: value,
        errorNameParentNode: false
      })
    } else {
      this.setState({
        errorNameParentNode: true
      })
    }
  }

  _goBack = () => {
    deleteDomain();
    this.props.history.goBack();
  };

  render() {
    const getNodeKey = ({ treeIndex }) => treeIndex;
    const {
      nameChildNode,
      errorNameParentNode,
      errorNameChildNode,
      errorSubtitleNode,
      subtitleNode,
    } = this.state;

    return (
      <div className='tree-container'>
        <div className='manipulation-block'>
          <div className='content-row'>
            <div className='primaryBtn' onClick={this._goBack}>Go Back</div>
          </div>
          <div className='content-row'>
            <div className={`input-field ${errorNameParentNode ? 'border-error' : ''}`}>
              <input type='text' onChange={this._handleChangeNameParentNode}/>
            </div>
            <div className='primaryBtn' onClick={this._AddNewNode}>Add parent node</div>
          </div>
          <div className='content-row'>
            <h5>Child node name:</h5>
            <div className={`input-field ${errorNameChildNode ? 'border-error' : ''}`}>
              <input type='text' onChange={this._handleChangeTextInput}/>
            </div>
          </div>
          <div className='content-row'>
            <h5>TestField:</h5>
            <div className={`input-field ${errorSubtitleNode ? 'border-error' : ''}`}>
              <input type='text' onChange={this._handleChangeSubtitleInput}/>
            </div>
          </div>
        </div>

        <SortableTree
          treeData={this.state.treeData}
          onChange={treeData => this.setState({ treeData })}
          onMoveNode={() => this.sendData(this.state.treeData)}
          generateNodeProps={({ node, path }) => ({
            listIndex: 0,
            lowerSiblingCounts: [],
            buttons: [
              <div
                className='primaryBtn'
                onClick={ () => {
                  if(!errorNameChildNode) {
                    this.setState(state => ({
                      treeData: addNodeUnderParent({
                        treeData: state.treeData,
                        parentKey: path[path.length - 1],
                        expandParent: true,
                        getNodeKey,
                        newNode: {
                          title: nameChildNode,
                          subtitle: '',
                          expanded: true
                        },
                      }).treeData,
                    }), () => this.sendData(this.state.treeData));

                  }
                }}
              >
                Add Child
              </div>,
              <div
                className='primaryBtn'
                onClick={() => {
                  this.setState(state => ({
                    treeData: removeNodeAtPath({
                      treeData: state.treeData,
                      path,
                      getNodeKey,
                    }),
                  }), () => this.sendData(this.state.treeData));
                }}
              >
                Remove
              </div>,
              <div
                className={`primaryBtn ${node.subtitle ? 'notVisible' : ''}`}
                onClick={() =>
                {this.setState(state => ({
                  treeData: changeNodeAtPath({
                    treeData: state.treeData,
                    path,
                    getNodeKey,
                    newNode: { ...node, subtitle: subtitleNode},
                  }),
                }), () => this.sendData(this.state.treeData));
                }}

              >
                Add TestField
              </div>,
            ],
          })}
          theme={FileExplorerTheme}
        />
      </div>
    );
  }
}