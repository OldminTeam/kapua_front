import { API_URL } from "./config";


export function fetchData(path) {

  let fullUrl = '';
  if(path) {
    fullUrl = `http://${API_URL}/${path}`
  }

  if (getDomain()) {
    fullUrl = `http://${getDomain()}.${API_URL}/tree/`;
  }

  return new Promise((resolve, reject) => {
    fetch(fullUrl)
      .then((response) => {
        return response.json().then((json) => {
          response.ok ? resolve(json) : reject(json);
        })
      })
  })
}

export function createObject(obj, path) {

  let fullUrl = '';
  if(getDomain()) {
    fullUrl = `http://${getDomain()}.${API_URL}/tree/`;
  }

  if(path) {
    fullUrl = `http://${API_URL}/${path}`
  }


  let headers = {
    'Accept' : 'application/json',
    'Content-Type': 'application/json'
  };

  return new Promise((resolve, reject) => {
    fetch(fullUrl, {
      method: 'POST',
      body: JSON.stringify(obj),
      headers: headers
    })
      .then((response) => {
        if (response.status === 201) {
          resolve(true);
        } else {
          return response.json().then((json) => {
            response.ok ? resolve(json) : reject(json);
          })
        }
      });
  });
}

export function setDomain(domain) {
  localStorage.setItem('domain', domain);
}

export function getDomain() {
  return localStorage.getItem('domain')
}

export function deleteDomain() {
  localStorage.removeItem('domain')
}
